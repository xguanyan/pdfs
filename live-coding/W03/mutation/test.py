import unittest

if __name__ == "__main__":
  name = "mutation.mutation_tests"
  suite = unittest.defaultTestLoader.loadTestsFromNames([name])
  result = unittest.TextTestRunner().run(suite)
