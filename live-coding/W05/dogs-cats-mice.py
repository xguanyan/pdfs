from z3 import *

d, c, m = Ints('d c m')
cst1 = (d + c + m == 100)

sol = Solver()
sol.add (cst1)
print (sol)

sol.check()
print (sol.model())

sol.add(c > 0, m > 0, d > 0)
print (sol)
sol.check()
print (sol.model())

cst2 = (1500 * d + 25 * m + 100 * c == 10000)
sol.add(cst2)
print (sol)

sol.check()
print (sol.model())

sol.add(Or(d != 3, m != 56, c != 41))
print (sol.check())
print (sol.model())

